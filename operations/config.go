package operations

import (
	"os"
	"strings"
)

// called upon package initialization. It cannot be called directly.
func init() {
	if isTestRun() {
		return
	}

	// in order to disable auto-backup we need to stop any running backup
	// if err := timeMachine.StopBackup(); err != nil {
	//	log.Fatal(err)
	//	return
	// }

	// FIXME this doesn't work with cron and launchd. Consider making this a cmd in its own right

	// if err := timeMachine.DisableAutoBackup(); err != nil {
	//	log.Fatal(err)
	// }
}

func isTestRun() bool {
	return strings.HasSuffix(os.Args[0], ".test")
}
