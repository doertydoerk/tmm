package operations

import (
	"log"

	"gitlab.com/doertydoerk/tmm/timeMachine"
)

func Mount() error {
	volumeName, err := getVolumeName()
	if err != nil {
		return err
	}

	if timeMachine.VolumeIsAttached(volumeName) {
		log.Print("volume is already mounted")
		return nil
	}

	log.Print("mounting Time Machine volume.")

	return timeMachine.MountVolume(volumeName)
}

func Unmount() error {
	volumeName, err := getVolumeName()
	if err != nil {
		return err
	}

	if !timeMachine.VolumeIsAttached(volumeName) {
		log.Print("volume is already unmounted")
		return nil
	}

	log.Print("unmounting Time Machine volume.")

	return timeMachine.UnmountVolume(volumeName)
}
