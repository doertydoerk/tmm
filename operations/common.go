package operations

import (
	"errors"
	"fmt"
	"strings"

	"gitlab.com/doertydoerk/tmm/util"
)

var (
	volumeNotFoundError = errors.New("volume not found")
)

func getVolumeName() (string, error) {
	cmd := "tmutil destinationinfo | grep 'Name' | awk '{print $3}'"
	result, err := util.ExecBashCmd(cmd)
	if err != nil {
		return "", err
	}

	if result == "" {
		return "", volumeNotFoundError
	}

	return strings.TrimSuffix(result, "\n"), nil
}

func getVolumeUuid(volumeName string) (string, error) {
	cmd := fmt.Sprintf("diskutil info %s | grep 'Volume UUID' | awk '{print $3}'", volumeName)
	result, err := util.ExecBashCmd(cmd)
	if err != nil {
		return "", err
	}

	if result == "" {
		return "", volumeNotFoundError
	}

	return strings.TrimSuffix(result, "\n"), nil
}

func getVolumeIdentifier(volumeName string) (string, error) {
	cmd := fmt.Sprintf("diskutil info %s | grep Node | awk '{print $3}'", volumeName)
	result, err := util.ExecBashCmd(cmd)
	if err != nil {
		return "", err
	}

	if result == "" {
		return "", volumeNotFoundError
	}

	return strings.TrimSuffix(result, "\n"), nil
}
