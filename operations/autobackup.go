package operations

import "gitlab.com/doertydoerk/tmm/timeMachine"

func AutoBackup(enable bool) error {
	if enable {
		return timeMachine.EnableAutoBackup()
	}

	return timeMachine.DisableAutoBackup()
}
