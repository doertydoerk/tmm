package operations

import (
	"fmt"
	"os"
	"time"

	"github.com/briandowns/spinner"
	"gitlab.com/doertydoerk/tmm/timeMachine"
)

var (
	oldStatus    = ""
	defaultDelay = 5 * time.Second
	progress     = spinner.New(spinner.CharSets[14], 100*time.Millisecond, spinner.WithWriter(os.Stderr))
)

func BackupNow(backupSnapshots bool) error {
	updateStatus("preparing backup")

	volumeName, err := getVolumeName()
	if err != nil {
		return fmt.Errorf("error preparing backup: %v", err)
	}

	defer func() {
		updateStatus("unmounting volume")
		if err := timeMachine.UnmountVolume(volumeName); err != nil {
			fmt.Println(err)
		}

		progress.Stop()
		fmt.Println("Done!")
	}()

	progress.Start()

	if !backupSnapshots {
		updateStatus("deleting local snapshots")

		if err := timeMachine.DeleteAllLocalSnapshots(); err != nil {
			return fmt.Errorf("error deleting local snapshots: %v", err)
		}
	}

	if !timeMachine.VolumeIsAttached(volumeName) {
		updateStatus("mounting volume")

		if err = timeMachine.MountVolume(volumeName); err != nil {
			return fmt.Errorf("error mounting volume: %v", err)
		}
	}

	if err := timeMachine.StartBackup(); err != nil {
		return fmt.Errorf("error starting backup: %v", err)
	}

	time.Sleep(defaultDelay)

	for {
		isRunning, newStatus := timeMachine.BackupIsRunning()
		if !isRunning {
			break
		}

		if oldStatus != newStatus {
			updateStatus(newStatus)
			oldStatus = newStatus
		}

		time.Sleep(defaultDelay)
	}

	updateStatus("unmounting volume")

	return nil
}

func updateStatus(status string) {
	progress.Stop()

	if oldStatus != "" {
		fmt.Println("✔", oldStatus)
	}

	progress.Suffix = " " + status
	progress.Start()
	oldStatus = status
}
