package operations

import (
	"os"
	"testing"
)

func setupTest(t *testing.T) {
	if os.Getenv("CI_JOB_STAGE") != "never_run_on_CI" {
		t.Skip("skipping test on CI")
	}
}
