package operations

import (
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/doertydoerk/tmm/timeMachine"
)

const volumeUuid = "7D73D889-38E1-4055-BA55-F57C6E53DE91"

func TestTmDestinationInfoWithSudo(t *testing.T) {
	setupTest(t)
	actual, err := getVolumeName()
	assert.NoError(t, err)
	assert.True(t, strings.Contains(actual, timeMachine.DefaultVolumeName))
}

func TestGetVolumeIdentifier(t *testing.T) {
	setupTest(t)
	actual, err := getVolumeIdentifier(timeMachine.DefaultVolumeName)
	assert.NoError(t, err)
	assert.True(t, strings.Contains(actual, "/dev/disk"))
}

func TestVolumeUuid(t *testing.T) {
	setupTest(t)
	actual, err := getVolumeUuid(timeMachine.DefaultVolumeName)
	assert.NoError(t, err)
	assert.Equal(t, volumeUuid, actual)
}
