package timeMachine

import (
	"bytes"
	"fmt"
	"log"
	"strings"

	"gitlab.com/doertydoerk/tmm/util"
	"howett.net/plist"
)

/*
https://pkg.go.dev/howett.net/plist
https://apple.stackexchange.com/questions/162464/time-machine-progress-from-the-command-line
*/

const (
	DefaultVolumeName = "TimeMachine"
	backupNotRunning  = "BackupNotRunning"
)

type (
	Status struct {
		BackupPhase                     string   `plist:"BackupPhase,omitempty"`
		ChangedItemCount                int      `plist:"ChangedItemCount,omitempty"`
		ClientID                        string   `plist:"ClientID"`
		DateOfStateChange               string   `plist:"DateOfStateChange,omitempty"`
		DestinationID                   string   `plist:"DestinationID,omitempty"`
		DestinationMountPoint           string   `plist:"DestinationMountPoint,omitempty"`
		EventFractionOfTotalProgressBar string   `plist:"EventFractionOfTotalProgressBar,omitempty"`
		FractionDone                    string   `plist:"FractionDone,optional"`
		Progress                        Progress `plist:"Progress,omitempty"`
		Running                         int      `plist:"Running"`
		Stopping                        int      `plist:"Stopping,omitempty"`
		SizingFreePreflight             int      `plist:"sizingFreePreflight,omitempty"`
	}

	Progress struct {
		Percent             string `plist:"Percent"`
		RawPercent          string `plist:"_raw_Percent"`
		RawTotalBytes       int    `plist:"_raw_totalBytes"`
		Bytes               int    `plist:"bytes"`
		Files               int    `plist:"files"`
		SizingFreePreflight int    `plist:"sizingFreePreflight"`
		TotalBytes          int    `plist:"totalBytes"`
		TotalFiles          int    `plist:"totalFiles"`
	}
)

func unmarshalStatus(status string) (Status, error) {
	buf := bytes.NewReader([]byte(status))

	var parsedStatus Status
	decoder := plist.NewDecoder(buf)
	err := decoder.Decode(&parsedStatus)
	if err != nil {
		return parsedStatus, err
	}
	fmt.Println(parsedStatus)

	return parsedStatus, nil
}

func MountVolume(volumeName string) error {
	cmd := fmt.Sprintf("diskutil mount %s", volumeName)
	_, err := util.ExecBashCmd(cmd)
	return err
}

func UnmountVolume(volumeName string) error {
	cmd := fmt.Sprintf("diskutil unmount %s", volumeName)
	_, err := util.ExecBashCmd(cmd)
	return err
}

func VolumeIsAttached(volumeName string) bool {
	cmd := fmt.Sprintf("diskutil info /Volumes/%s | grep Node | awk '{print $3}'", volumeName)
	res, err := util.ExecBashCmd(cmd)
	return err == nil && res != ""
}

// StopBackup stops the currently running backup
func StopBackup() error {
	stop := "tmutil stopbackup"
	if _, err := util.ExecBashCmd(stop); err != nil {
		return fmt.Errorf("stopping current backup failed: %s", err.Error())
	}

	return nil
}

// EnableAutoBackup will enable automated backups, so you can do your own schedule.
func EnableAutoBackup() error {
	disable := "tmutil enable"
	if _, err := util.ExecBashCmdWithSudo(disable); err != nil {
		return fmt.Errorf("enabling auto-backup failed: %s", err.Error())
	}

	return nil
}

// DisableAutoBackup will disable automated backups.
func DisableAutoBackup() error {
	disable := "tmutil disable"
	if _, err := util.ExecBashCmdWithSudo(disable); err != nil {
		return fmt.Errorf("disabling auto-backup failed: %s", err.Error())
	}

	return nil
}

func DeleteAllLocalSnapshots() error {
	// https://apple.stackexchange.com/questions/340905/how-to-delete-all-local-timemachine-snapshots
	cmd := `for d in $(tmutil listlocalsnapshotdates | grep "-"); do tmutil deletelocalsnapshots $d; done`
	_, err := util.ExecBashCmd(cmd)
	return err
}

func StartBackup() error {
	_, err := util.ExecBashCmd("tmutil startbackup")
	return err
}

func BackupIsRunning() (bool, string) {
	status, err := util.ExecBashCmd("tmutil currentPhase")
	if err != nil {
		log.Print(err)
	}

	status = strings.TrimSuffix(status, "\n")
	return status != backupNotRunning, status
}
