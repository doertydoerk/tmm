package timeMachine

import (
	"fmt"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestParseTimeMachineStatus(t *testing.T) {
	_, err := unmarshalStatus(copyingStatus)
	assert.NoError(t, err)

	_, err = unmarshalStatus(findingStatus)
	assert.NoError(t, err)

	actual, err := unmarshalStatus(idleStatus)
	assert.NoError(t, err)

	fmt.Print(actual)
}

var copyingStatus = `{
    BackupPhase = Copying;
    ClientID = "com.apple.backupd";
    DateOfStateChange = "2022-02-19 22:40:43 +0000";
    DestinationID = "BC4BF904-D0B9-4C73-BB28-D00273849344";
    DestinationMountPoint = "/Volumes/TimeMachine";
    EventFractionOfTotalProgressBar = "0.1";
    Progress =     {
        Percent = "0.1186417302520979";
        "_raw_Percent" = "0.1186417302520979";
        "_raw_totalBytes" = 228478255104;
        bytes = 18063360;
        files = 2463;
        sizingFreePreflight = 1;
        totalBytes = 228478255104;
        totalFiles = 3239233;
    };
    Running = 1;
    Stopping = 0;
}`

var findingStatus = `{
    BackupPhase = FindingChanges;
    ChangedItemCount = 1123561;
    ClientID = "com.apple.backupd";
    DateOfStateChange = "2022-02-19 22:30:30 +0000";
    DestinationID = "BC4BF904-D0B9-4C73-BB28-D00273849344";
    DestinationMountPoint = "/Volumes/TimeMachine";
    EventFractionOfTotalProgressBar = "0.1";
    FractionDone = "0.4513420796565359";
    Running = 1;
    Stopping = 0;
    sizingFreePreflight = 1;
}`

const (
	defaultVolumeName = "TimeMachine"
)

var idleStatus = `{
    ClientID = "com.apple.backupd";
    Percent = 1;
    Running = 0;
}`

func setupTest(t *testing.T) {
	if os.Getenv("CI_JOB_STAGE") != "never_run_on_CI" {
		t.Skip("skipping test on CI")
	}
}

func TestDeleteAllSnapshot(t *testing.T) {
	setupTest(t)
	assert.NoError(t, DeleteAllLocalSnapshots())
}

func TestMountUnmount(t *testing.T) {
	setupTest(t)

	t.Run("mount", func(t *testing.T) {
		err := MountVolume(defaultVolumeName)
		assert.NoError(t, err)
	})

	t.Run("unmount", func(t *testing.T) {
		err := UnmountVolume(defaultVolumeName)
		assert.NoError(t, err)
	})
}

func TestBackupIsRunning(t *testing.T) {
	setupTest(t)

	ok, status := BackupIsRunning()
	assert.False(t, ok)
	assert.Equal(t, backupNotRunning, status)
}
