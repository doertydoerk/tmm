/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

*/
package main

import "gitlab.com/doertydoerk/tmm/cmd"

func main() {
	cmd.Execute()
}
