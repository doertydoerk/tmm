package cmd

import (
	"log"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/doertydoerk/tmm/operations"
)

var (
	unmountCmd = &cobra.Command{
		Use:   "unmount",
		Short: "Will unmount your Time Machine volume. No flags available for this command",
		Run: func(cmd *cobra.Command, args []string) {
			if err := operations.Unmount(); err != nil {
				log.Println(err.Error())
				os.Exit(1)
			}
		},
	}
)

func init() {
	rootCmd.AddCommand(unmountCmd)
}
