package cmd

import (
	"log"

	"github.com/spf13/cobra"
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "tmm",
	Short: "TimeMachineManager helps you manage your TimeMachine volunes and backups.",
	Long: `TimeMachineManager allows you to perform certain tasks on your TimeMachine volume. 
For example, it allows you to enable/disable auto-mount of the TimeMachine volume, 
lets you run a backup with mounting TimeMachine first and unmounting it when done.`,
	// Uncomment the following line if your bare application
	// has an action associated with it:
	// Run: func(cmd *cobra.Command, args []string) { },
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		log.Fatal(err.Error())
	}
}

func init() {
	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.

	// rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.gitlab.com/doertydoerk/time-machine-manager.yaml)")

	// Cobra also supports local flags, which will only run
	// when this action is called directly.

	//	rootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
