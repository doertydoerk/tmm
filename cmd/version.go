package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

var Version string

var (
	versionCmd = &cobra.Command{
		Use:   "version",
		Short: "returns the version of the Time Machine Manager",
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Println(Version)
		},
	}
)

func init() {
	rootCmd.AddCommand(versionCmd)
}
