package cmd

import (
	"log"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/doertydoerk/tmm/operations"
)

var (
	autoBackupKey = "autoBackup"

	autoBackupCmd = &cobra.Command{
		Use:   "autoBackup",
		Short: "Enables/disables Time Machine's automatic backup",
		Run: func(cmd *cobra.Command, args []string) {
			enable, err := cmd.Flags().GetBool(autoBackupKey)
			if err != nil {
				enable = false
			}

			if err := operations.AutoBackup(enable); err != nil {
				log.Println(err.Error())
				os.Exit(1)
			}
		},
	}
)

func init() {
	rootCmd.AddCommand(autoBackupCmd)
	autoBackupCmd.Flags().BoolP(autoBackupKey, "a", true, "'false' disables and 'true' enables Time Machine's automatic backup.")
}
