package cmd

import (
	"log"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/doertydoerk/tmm/timeMachine"
)

var (
	clean = &cobra.Command{
		Use:   "clean",
		Short: "deletes all local snapshots that reside on the Mac",
		Run: func(cmd *cobra.Command, args []string) {
			if err := timeMachine.DeleteAllLocalSnapshots(); err != nil {
				log.Println(err.Error())
				os.Exit(1)
			}
		},
	}
)

func init() {
	rootCmd.AddCommand(clean)
}
