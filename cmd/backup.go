package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/doertydoerk/tmm/operations"
)

var (
	backupSnapshotsKey = "backupSnapshots"

	backupCmd = &cobra.Command{
		Use:   "backup",
		Short: "Launch a backup. Do 'tmm backup -h' for details",
		Run: func(cmd *cobra.Command, args []string) {
			snapshots, err := cmd.Flags().GetBool(backupSnapshotsKey)
			if err != nil {
				snapshots = false
			}

			if err := operations.BackupNow(snapshots); err != nil {
				fmt.Println(err)
				os.Exit(1)
			}
		},
	}
)

func init() {
	rootCmd.AddCommand(backupCmd)
	backupCmd.Flags().BoolP(backupSnapshotsKey, "s", true, "if false local snapshots will be deleted before backup commences.")
}
