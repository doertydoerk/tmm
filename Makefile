VERSION?=$(shell git describe --tags --always)

.PHONY: write_version

write_version:
	@echo $(VERSION) > operations/release.txt

build_arm64:
	@echo "compiling arm64 binary..."
	@mkdir -p ./build/$(VERSION)/arm64
	@env GOOS=darwin GOARCH=arm64 /usr/local/go/bin/go build -o ./build/$(VERSION)/arm64/tmm

build_amd64:
	@echo "compiling amd64 binary..."
	@mkdir -p ./build/$(VERSION)/amd64
	@env GOOS=darwin GOARCH=amd64 /usr/local/go/bin/go build -o ./build/$(VERSION)/amd64/tmm

build: write_version build_amd64 build_arm64
	@echo "Done!"