package util

import (
	"os"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func setupTest(t *testing.T) {
	if os.Getenv("CI_JOB_STAGE") != "never_run_on_CI" {
		t.Skip("skipping test on CI")
	}
}

func TestRetrievePassword(t *testing.T) {
	setupTest(t)
	testKey := "tmm_test_password"
	testPassword := "foo"

	actual, err := GetPassword(testKey)
	assert.NoError(t, err)
	assert.Equal(t, testPassword, strings.TrimSuffix(actual, "\n"))
}
