package util

import (
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestExecBashCmdWithSudo(t *testing.T) {
	setupTest(t)

	t.Run("no sudo command", func(t *testing.T) {
		actual, err := ExecBashCmdWithSudo("cat ../main.go")
		assert.NoError(t, err)
		assert.True(t, strings.Contains(actual, "gitlab.com/doertydoerk/time-machine-manager/cmd"))
	})

	t.Run("any sudo command", func(t *testing.T) {
		actual, err := ExecBashCmdWithSudo("cat ../main.go")
		assert.NoError(t, err)
		assert.True(t, strings.Contains(actual, "gitlab.com/doertydoerk/time-machine-manager/cmd"))
	})

	t.Run("requires sudo & FULL DISK ACCESS", func(t *testing.T) {
		_, err := ExecBashCmdWithSudo("tmutil disable")
		assert.NoError(t, err)
	})
}
