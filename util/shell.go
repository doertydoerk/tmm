package util

import (
	"os/exec"
	"strings"
)

func ExecBashCmd(cmd string) (string, error) {
	result, err := exec.Command("/bin/bash", "-c", cmd).Output()
	if err != nil {
		return "", nil
	}

	return string(result), nil
}

// ExecBashCmdWithSudo requires Full Disk Access in System Settings > Security
func ExecBashCmdWithSudo(cmd string) (string, error) {
	// https://stackoverflow.com/questions/68253600/how-to-use-golang-to-shell-command-with-given-sudo-password
	pw, err := GetPassword(passwordKey)
	if err != nil {
		return "", nil
	}

	command := exec.Command("sudo", "-S", "/bin/sh", "-c", cmd)
	command.Stdin = strings.NewReader(pw)
	out, err := command.CombinedOutput()
	return string(out), err
}
