package util

import "fmt"

const passwordKey = "tmm_password"

// FIXME maybe no longer needed
func GetPassword(key string) (string, error) {
	// https://medium.com/@johnjjung/how-to-store-sensitive-environment-variables-on-macos-76bd5ba464f6
	//TODO may return a more telling error message
	cmd := fmt.Sprintf("security find-generic-password -a \"$USER\" -s '%s' -w", key)
	return ExecBashCmd(cmd)
}
