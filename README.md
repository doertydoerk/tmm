# Time-Machine-Manager (tmm)

A [Cobra](https://github.com/spf13/cobra/blob/master/cobra/README.md) based CLI-tool to help manage macOS Time Machine.

Tested on macOS Sequoia 15.3.1 (24D70). Other versions may be working but have not been tested.

## Use Case
Every time I unplugged my computer from the dock, I had to either to manually unmount the volume or get a nasty notification
that I should have unmounted the volume first.
Also, my company issued antivirus was driving me crazy as it never ignored (although it should have) my Time Machine drive and
kept asking whether to scan Time Machine backups and local snapshots. 

So, the use case is to have the Time Machine volume attached to the computer/dock at all times but only to mount it as needed.
That would allow for manual or scheduled backups (e.g. once a day at night in my case via cron) and not having to worry about
mounting/unmounting the drive.
 
## In-Scope
- backup work-flow from the command line (see 'Usage section')
  - option to delete local snapshots
- manually mount/unmount Time Machine volume
 
## Out-Of-Scope
- any restore operation
- enable/disable auto-mount Time Machine volume

## Installation
Easiest way (recommended) to install, when you have [Go](https://go.dev) installed on your machine, is to do:
```shell
go install gitlab.com/doertydoerk/tmm@latest
````
Or you can download the pre-built binary for your platform [here](https://gitlab.com/doertydoerk/tmm/-/releases). This, 
however, comes with two additional steps:
- you need to make the binary executable by running `chmod +x tmm`
- since the app is not notarized, you need to disable gatekeeper for the app following these [instruction](https://macpaw.com/how-to/fix-macos-cannot-verify-that-app-is-free-from-malware). 

## Usage
Make sure that a volume is known as your backup volume by the Time Machine app in the settings section, so Time-Machine-Manager
can figure out what volume to perform an operation on. At the end of any command you may attach '-h' for help.

To perform a backup run:
```shell
tmm backup [-s=true | false]
````
This will perform the following steps:
- find the Time Machine volume
- delete Time Machine snapshots,if `-s=false` is provided
- mount volume if unmounted
- perform full backup
- unmount volume

By default, local snapshots will be backed up. So, if you don't want that, you may provide the `-s=false` flag (default = true).

To mount/unmount the Time Machine volume run:
```shell
tmm [mount | unmount]

````
For general help on how to do things just run:
```shell
tmm

````
